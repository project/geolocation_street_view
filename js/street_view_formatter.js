/**
 * @file
 * Geolocation Street View formatter Javascript.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.geolocation.streetViewFormatter = Drupal.geolocation.streetViewFormatter || {};

  /**
   * Attach Google Street View formatter functionality.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches Google Street View formatter functionality to relevant elements.
   */
  Drupal.behaviors.geolocationStreetViewFormatter = {
    attach: function (context, drupalSettings) {
      if (typeof Drupal.geolocation.google.addLoadedCallback === 'function') {
        Drupal.geolocation.google.addLoadedCallback(function () {
          Drupal.geolocation.streetViewFormatter.attach(context);
        });

        // Load Google Maps API and execute all callbacks.
        Drupal.geolocation.google.load();
      }
    }
  };

  /**
   * @param {HTMLElement} context Context
   */
  Drupal.geolocation.streetViewFormatter.attach = function (context) {
    $('.geolocation-map-wrapper', context).once('geolocation-google-maps-street-view-formatter-processed').each(function (index, item) {
      var mapWrapper = $(item);

      // Open Street View when POV is given.
      if (mapWrapper.is('[data-map-heading][data-map-pitch][data-map-zoom]')) {
        var mapId = mapWrapper.attr('id').toString();
        var map = Drupal.geolocation.getMapById(mapId);
        if (!map) {
          return;
        }

        // Street View.
        var panorama = map.googleMap.getStreetView();

        // Configure Street View options.
        panorama.setOptions({
          addressControl: true,
          enableCloseButton: false
        });

        // Set position.
        var position = map.mapMarkers[0].position;
        panorama.setPosition(position);

        // Set POV.
        panorama.setPov({
          heading: Number(mapWrapper.data('map-heading')),
          pitch: Number(mapWrapper.data('map-pitch')),
          zoom: Number(mapWrapper.data('map-zoom'))
        });
        panorama.setVisible(true);
      }
    });
  }

})(jQuery, Drupal);
