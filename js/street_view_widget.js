/**
 * @file
 * Geolocation Street View widget Javascript.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.geolocation.streetViewWidget = Drupal.geolocation.streetViewWidget || {};

  /**
   * Attach Google Street View widget functionality.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches Google Street View widget functionality to relevant elements.
   */
  Drupal.behaviors.geolocationStreetViewFormatter = {
    attach: function (context, drupalSettings) {
      if (typeof Drupal.geolocation.google.addLoadedCallback === 'function') {
        Drupal.geolocation.google.addLoadedCallback(function () {
          Drupal.geolocation.streetViewWidget.attach(context);
        });

        // Load Google Maps API and execute all callbacks.
        Drupal.geolocation.google.load();
      }
    }
  };

  /**
   * @param {HTMLElement} context Context
   */
  Drupal.geolocation.streetViewWidget.attach = function (context) {
    $('.geolocation-map-widget', context).once('geolocation-google-maps-street-view-widget-processed').each(function (index, item) {
      var widgetId = $(item).attr('id').toString();
      var widget = Drupal.geolocation.widget.getWidgetById(widgetId);
      if (!widget) {
        return;
      }

      // Street View.
      var panorama = widget.map.googleMap.getStreetView();

      // POV field values.
      var heading = $('.geolocation-hidden-heading', item).attr('value');
      var pitch = $('.geolocation-hidden-pitch', item).attr('value');
      var zoom = $('.geolocation-hidden-zoom', item).attr('value');

      // Open Street View when POV is given.
      if (!isNaN(parseFloat(heading)) && !isNaN(parseFloat(pitch)) && !isNaN(parseFloat(zoom))) {
        var position = false;
        widget.getAllInputs().each(function (delta, input) {
          var coordinates = widget.getCoordinatesByInput(input);
          if (!coordinates) {
            return;
          }
          position = new google.maps.LatLng({
            lat: parseFloat(coordinates.lat),
            lng: parseFloat(coordinates.lng)
          });
        });
        if (position) {
          panorama.setPosition(position);
        }
        panorama.setPov({
          heading: parseFloat(heading),
          pitch: parseFloat(pitch),
          zoom: parseFloat(zoom)
        });
        panorama.setVisible(true);
      }

      // Add the position and POV listeners.
      panorama.addListener('position_changed', function () {
        var position = panorama.getPosition();
        widget.locationAlteredCallback('auto-client-location', {lat: position.lat(), lng: position.lng()}, null);
      });
      panorama.addListener('pov_changed', function () {
        Drupal.geolocation.streetViewWidget.setHiddenInputFields(panorama.getPov(), item);
      });

      // Clear POV fields when location is changed or cleared.
      Drupal.geolocation.geocoder.addResultCallback(function (address) {
        Drupal.geolocation.streetViewWidget.clearHiddenInputFields(item);
      }, widgetId);
      Drupal.geolocation.geocoder.addClearCallback(function () {
        Drupal.geolocation.streetViewWidget.clearHiddenInputFields(item);
      }, widgetId);
    });
  };

  /**
   * Set the Street View input fields.
   *
   * @param {StreetViewPov} pov - The POV from Street View.
   * @param {HTMLElement} widget - The widget element.
   */
  Drupal.geolocation.streetViewWidget.setHiddenInputFields = function (pov, widget) {
    $('.geolocation-hidden-heading', widget).attr('value', pov.heading);
    $('.geolocation-hidden-pitch', widget).attr('value', pov.pitch);
    $('.geolocation-hidden-zoom', widget).attr('value', pov.zoom);
  };

  /**
   * Clear the Street View input fields.
   *
   * @param {HTMLElement} widget - The widget element.
   */
  Drupal.geolocation.streetViewWidget.clearHiddenInputFields = function (widget) {
    $('.geolocation-hidden-heading', widget).attr('value', '');
    $('.geolocation-hidden-pitch', widget).attr('value', '');
    $('.geolocation-hidden-zoom', widget).attr('value', '');
  };

})(jQuery, Drupal);
