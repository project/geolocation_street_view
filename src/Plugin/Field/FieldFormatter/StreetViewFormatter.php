<?php

namespace Drupal\geolocation_street_view\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Render\Element;
use Drupal\geolocation\Plugin\Field\FieldFormatter\GeolocationMapFormatterBase;

/**
 * Plugin implementation of the 'geolocation_street_view' formatter.
 *
 * @FieldFormatter(
 *   id = "geolocation_street_view",
 *   label = @Translation("Geolocation Street View"),
 *   field_types = {
 *     "geolocation"
 *   }
 * )
 */
class StreetViewFormatter extends GeolocationMapFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if ($items->count() == 0) {
      return [];
    }

    $elements = parent::viewElements($items, $langcode);

    if (!empty($items->get(0))
      && !empty($items->get(0)->getValue()['data']['google_street_view_pov'])
      && is_array($items->get(0)->getValue()['data']['google_street_view_pov'])
    ) {
      $street_view_pov = $items->get(0)->getValue()['data']['google_street_view_pov'];
      foreach (Element::children($elements) as $delta => $element) {
        $elements[$delta]['#attached']['library'][] = 'geolocation_street_view/formatter.street_view';
        $elements[$delta]['#attributes']['data-map-heading'] = $street_view_pov['heading'];
        $elements[$delta]['#attributes']['data-map-pitch'] = $street_view_pov['pitch'];
        $elements[$delta]['#attributes']['data-map-zoom'] = $street_view_pov['zoom'];
      }
    }

    return $elements;
  }

}
